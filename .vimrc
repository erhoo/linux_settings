filetype indent on
set ai
set hlsearch
"set mouse=a
set incsearch
set confirm
set number
set smartcase
set wildmenu
set smartindent  
set wildmode=list:longest,full
set tabstop=2 shiftwidth=2 expandtab
"set spelllang=en_gb spell
"set spell spelllang=en_us
syntax on
autocmd FileType plaintex,tex,latex syntax spell toplevel
autocmd BufNew,BufRead SConstruct setf python

"======================
" Key Maps
"======================
map <Presse Art-N> set nonumber


"colorscheme desert
"colorscheme molokai
colorscheme monokai
"let g:molokai_original = 1
"let g:rehash256 = 1
"colorscheme railscasts
"set background=dark
"colorscheme dracula

"set background=dark
"let g:solarized_termcolors=   256
"let g:solarized_termtrans =   1
"let g:solarized_contrast  =   "high"
"let g:solarized_visibility=   "high"
"colorscheme solarized

set t_Co=256

"let fts = ['c', 'cpp', 's', 'h', 'o', 'cc', 'hh']
"if index(fts, &filetype) == -1
"   colorscheme desert
"endif
"
if exists('+colorcolumn')
	set colorcolumn=87
else
	au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1) 
endif

"===== key mapping ======
map <F1> v]}zf "folding 
map <F2> zo "folding unlock
map <F3> :Tlist<cr><C-W><C-W> “taglist
map <F4> :BufExplorer<cr> “BufExplorer
map <PageUp> <C-U><C-U>
map <PageDown> <C-D><C-D>

"===== ctags setting ======
set tags=./tags
set csprg=/usr/local/bin/cscope
set nocsverb
cs add ./cscope.out
set csverb
set csto=0
set cst 

if version >= 500 
	func! Sts()
		let st = expand("<cword>")
		exe "sts ".st
	endfunc
	nmap ,st :call Sts()<cr>  

	func! Tj()
		let st = expand("<cword>")
		exe "tj ".st
	endfunc
	nmap ,tj :call Tj()<cr>
endif

"===== cscope setting =====
set csprg=/usr/bin/cscope
set csto=0
set cst 
set nocsverb

if filereadable("./cscope.out")
	cs add cscope.out
else
	cs add /usr/src/linux-2.4/cscope.out
endif
set csverb

func! Css()
	let css = expand("<cword>")
	new 
	exe "cs find s ".css
	if getline(1) == ""  
		exe "q!"
	endif
endfunc
nmap ,css :call Css()<cr>

func! Csc()
	let csc = expand("<cword>")
	new 
	exe "cs find c ".csc
	if getline(1) == ""
		exe "q!"
	endif
endfunc
nmap ,csc :call Csc()<cr>

func! Cse()
	let cse = expand("<cword>")
	new
	exe "cs find e ".cse
	if getline(1) == ""
		exe "q!"
	endif
endfunc
nmap ,cse :call Cse()<cr>

"===== man page setting  =====
func! Man()
	let sm = expand("<cword>")
	exe "!man –S 2:3:4:5:6:7:8:9:tcl:n:l:p:o ".sm
endfunc
nmap ,ma :call Man()<cr><cr>

" -----------------------------"
" Source Explorer Configuration
" -----------------------------"

nmap <C-P> :SrcExplToggle<CR>
nmap <C-H> <C-W>h
nmap <C-J> <C-W>j
nmap <C-K> <C-W>k
nmap <C-L> <C-W>l
let g:SrcExpl_winHeight = 9
let g:SrcExpl_refreshTime = 100
let g:SrcExpl_jumpKey = "<ENTER>"
let g:SrcExpl_gobackKey = "<SPACE>"
let g:SrcExpl_isUpdateTags = 0


" -----------------------------"
" Pathogen
" -----------------------------"

execute pathogen#infect()
filetype plugin indent on

" -----------------------------"
" Syntacstic 
" -----------------------------"

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

"let g:syntastic_java_javac_classpath = ".:./SaM-2.6.2.jar"
"let g:syntastic_cpp_compiler = 'clang'

let g:syntastic_cpp_compiler = "g++"
let g:syntastic_cpp_compiler_options = "-std=c++11 -Wall -Wextra -Wpedantic"
let g:syntastic_tex_checkers = ['lacheck']

" Show only python errors, diable warning
let g:syntastic_python_checkers = ['pylint']  "" or ['flake8', 'pylint'], etc
let g:syntastic_python_pylint_args = '-E'
"" to show it accepts a string of args, also:
let g:syntastic_python_pylint_args = '--rcfile=/path/to/rc -E'

" -----------------------------"
" indentline => https://github.com/Yggdroot/indentLine
" -----------------------------"
let g:indentLine_color_term = 239
let g:indentLine_char = '|'

" -----------------------------"
" Ctrlp => https://github.com/kien/ctrlp.vim
" -----------------------------"
"let g:ctrlp_cmd = 'CtrlP'




