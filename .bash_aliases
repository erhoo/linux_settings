#!/bin/sh
# Short Cuts
alias ls='ls -GFh'
alias ll='ls -l -G'
alias la='ls -a -G'
alias md='mkdir'
alias c='clear'							    
alias ..='cd ..'

# Directories
alias sim="cd /Volumes/DATA/99_LAB"
alias page="cd /Volumes/DATA/99_LAB/90_mypage/erhoo82.github.io"
alias ext="cd /Volumes/EXT_DATA/Research"
alias wk="cd /mnt/backups"

# Projects
alias mbs="cd /mnt/backups/00_Research/07_Topics/01_DL/mbs"
alias spar="cd /mnt/backups/00_Research/07_Topics/01_DL/spar"
alias ff="cd /mnt/backups/00_Research/01_Projects/00_fault_injector/hamartia_suite"

# Access
alias daisy="ssh -X lym@daisy.ece.utexas.edu" 
alias mario="ssh -X -Y lym@mario.ece.utexas.edu" 
alias dlxp="ssh -X -Y sklym@10.157.92.254"

#alias stemp="ssh -X -Y erhoo@stampede.tacc.utexas.edu" #stemped server
alias stp2="ssh -X erhoo@stampede2.tacc.utexas.edu" #stemped server
alias knl="ssh -X erhoo@login-knl1.stampede.tacc.utexas.edu" #stemped server
alias ls5="ssh -X -Y erhoo@ls5.tacc.utexas.edu" #stemped server
alias mav="ssh -X erhoo@maverick.tacc.utexas.edu" #stemped server

# Bianries
alias tb="tensorboard --logdir=. --host localhost --port 8088"
alias sb="ssh siavash@maverick2.tacc.utexas.edu"

